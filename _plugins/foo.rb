module Foo
  class Generator < Jekyll::Generator
    def generate(site)
      puts "Our plugin is running"
      puts site.pages.inspect
      puts site.posts.inspect

      site.pages.each do |page|
        page.data['foo'] = 'bar'
      end
      site.posts.each do |post|
        post.data['foo'] = 'bar'
      end
    end
  end
end